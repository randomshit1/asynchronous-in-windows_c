#include <windows.h>
#include <stdio.h>

DWORD WINAPI thread_function(LPVOID lpParam) // Modul yang ingin dibuat async
{
    // Algoritma yang ingin dibuat async
    printf("Hello from a new thread!\n");
    for (size_t i = 0; i < 100; i++)
    {
        printf("%d iii", i);
        Sleep(100);
    }
    ExitThread(0); // Hentikan async
    return 0;
}

int main()
{

    //============== Algoritm ==============
    HANDLE thread = CreateThread(NULL, 0, thread_function, NULL, 0, NULL); // Register modul async ke dalam memory
    if (thread == NULL)
    {
        printf("Error creating thread\n");
        return 1;
    }
    WaitForSingleObject(thread, 0); // wait for the thread to finish, 0 agar tidak saling menunggu (bisa eksekusi selagi scanf)
    CloseHandle(thread);            // Menghapus modul async dari memory

    //============== Algoritm ==============

    // Algoritma dibawah ini akan berjalan bersamaan dengan modul async
    printf("Hello from the main thread\n");
    for (size_t i = 0; i < 100; i++)
    {
        printf("[[%d]]", i);
        Sleep(100);
    }

    return 0;
}
